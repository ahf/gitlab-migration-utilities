This script makes sure that all external users have a limit of 5 projects and
cannot create groups.

This script was used to solve:
https://gitlab.torproject.org/tpo/tpa/gitlab/-/issues/52
