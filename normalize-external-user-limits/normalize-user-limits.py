#!/usr/bin/env python3

import gitlab
import sys
import pprint

GITLAB_URL = "https://gitlab.torproject.org/"
PRIVATE_TOKEN = ""

gitlab_client = gitlab.Gitlab(GITLAB_URL, PRIVATE_TOKEN, api_version=4)

gitlab_client.auth()

user = gitlab_client.user
print("Logged in as: {}".format(user.username))

c = 0

for u in gitlab_client.users.list(external=True, all=True):
    if not u.external:
        print("SKIP")
        continue

    c += 1

    print("Handling: {}".format(u.username))

    # Specify settings here.
    u.can_create_group = False
    u.projects_limit = 5
    u.save()

print("Changed {} users".format(c))
