# Gitlab Migration Utilities

This repository contains the small scripts that was needed during and after the
migration from Tor's Trac to Gitlab. Many of these scripts are written in a
hurry to get a very specific problem solved and may seem strange :-)
