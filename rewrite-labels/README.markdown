This script was used to rewrite a lot of labels from our Trac import to how we
wanted the labels to be in Gitlab.

The input files for this script can be found in:
https://gitlab.torproject.org/ahf/label-cleanup/
