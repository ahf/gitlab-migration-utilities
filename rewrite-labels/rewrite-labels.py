#!/usr/bin/env python3

import gitlab
import sys
import pprint
import yaml

global_data = None

with open("label-cleanup/global.yaml") as f:
    global_data = yaml.safe_load(f)

assert global_data is not None

GITLAB_URL = "https://gitlab.torproject.org/"
PRIVATE_TOKEN = ""

gitlab_client = gitlab.Gitlab(GITLAB_URL, PRIVATE_TOKEN, api_version=4)

gitlab_client.auth()

user = gitlab_client.user
print("Logged in as: {}".format(user.username))

projects = set()

for project in gitlab_client.projects.list(all=True):
    if project.path_with_namespace.startswith("tpo/"):
        projects.add(project)

global_labels_to_reassign = global_data["reassign"]
global_labels_to_save = set(global_data["save"] + list(global_labels_to_reassign.values()))

# pprint.pprint(global_labels_to_reassign)
# pprint.pprint(global_labels_to_save)

tpo_group = gitlab_client.groups.get(268)
tpo_labels = set()

for label in tpo_group.labels.list(all=True):
    tpo_labels.add(label.name)

missing_labels = False

for label in global_labels_to_reassign.values():
    if label not in tpo_labels:
        print("Warning: {} missing in TPO".format(label))
        missing_labels = True

if missing_labels:
    sys.exit(1)

ignore_projects = set([
    "tpo/anti-censorship/pluggable-transports/snowflake-mobile",
    "tpo/anti-censorship/pluggable-transports/snowflake-webext",
    "tpo/anti-censorship/team",
    "tpo/core/shadow",
    "tpo/core/torspec",
    "tpo/tpa/wiki-archive",
    "tpo/tpa/wiki-infra-archive",
])

result = {}

for project in projects:
    if project.path_with_namespace in ignore_projects:
        continue

    if project.path_with_namespace == "tpo/core/chutney":
        continue

    result = {}

    result_labels_saved = set()
    result_labels_deleted = set()

    print("Looking into: {}".format(project.path_with_namespace))

    labels_to_delete = {}
    labels_to_save = set()

    input_data = {
        "project":  "",
        "reassign": {},
        "save":     [],
        "delete":   [],
        "unsorted": [],
    }

    try:
        with open("label-cleanup/{}.yaml".format(project.path_with_namespace)) as f:
            input_data = yaml.safe_load(f)

            assert len(input_data) == 1
            input_data = input_data[0]

            if "delete" not in input_data or input_data["delete"] is None:
                input_data["delete"] = []

            if "unsorted" not in input_data or input_data["unsorted"] is None:
                input_data["unsorted"] = []

            if "reassign" not in input_data or input_data["reassign"] is None:
                input_data["reassign"] = {}

            if "save" not in input_data or input_data["save"] is None:
                input_data["save"] = []
    except Exception as e:
        print("Error: {}".format(e))

    check_labels_to_save = set(list(input_data["reassign"].values()) + input_data["save"])
    check_labels_to_delete = set(input_data.get("delete", []) + input_data.get("unsorted", []))

    bad_labels = set()

    for label in check_labels_to_save:
        if label in check_labels_to_delete:
            bad_labels.add(label)

    if len(bad_labels) > 0:
        print("Labels in delete that was also supposed to be saved: {}".format(bad_labels))

    labels_to_reassign = {}
    labels_to_save = set()

    for a, b in input_data["reassign"].items():
        assert a not in labels_to_reassign

        if a == b:
            labels_to_save.add(a)
        else:
            labels_to_reassign[a] = b
            labels_to_save.add(b)

    for a, b in global_labels_to_reassign.items():
        if a in labels_to_reassign:
            if labels_to_reassign[a] != b:
                print("Warning: Removing assignment: '{}' -> '{}' with '{}' -> '{}'".format(
                    a, labels_to_reassign[a], a, b))

        labels_to_reassign[a] = b
        labels_to_save.add(b)

    for label in input_data["save"]:
        labels_to_save.add(label)

    for label in global_labels_to_save:
        labels_to_save.add(label)

    # pprint.pprint(labels_to_reassign)
    # pprint.pprint(labels_to_save)

    project.emails_disabled = True
    project.save()
    print("Emails disabled")

    for issue in project.issues.list(all=True):
        if issue.iid >= 40000:
            print("Ignoring non-trac Ticket: {}".format(issue.iid))
            continue

        actionable = False
        labels_to_delete = set()
        labels_to_add = set()
        labels = set(issue.labels)

        for label in labels:
            if label in labels_to_reassign:
                actionable = True
                labels_to_delete.add(label)

                new_label = labels_to_reassign[label]

                if new_label not in labels:
                    labels_to_add.add(new_label)

                continue

            if label not in labels_to_save:
                actionable = True
                labels_to_delete.add(label)

        if actionable:
            new_labels = issue.labels[:]

            new_labels = [label for label in new_labels if label not in labels_to_delete]
            new_labels += list(labels_to_add)

            if len(labels_to_add) > 0 or len(labels_to_delete) > 0:
                print("{}#{}: Add: {}. Delete: {}. (Old: {}): {}".format(project.name, issue.iid, labels_to_add, labels_to_delete, issue.labels, new_labels))

            result[issue.iid] = { "before": issue.labels[:], "after": new_labels[:] }

            for label in new_labels:
                result_labels_saved.add(label)

            for label in labels_to_delete:
                result_labels_deleted.add(label)

            issue.labels = new_labels
            issue.save(sudo="TracBot")
        else:
            result[issue.iid] = { "before": issue.labels[:], "after": issue.labels[:] }

    project.emails_disabled = False
    project.save()
    print("Emails enabled")

    with open("label-cleanup/{}-history.yaml".format(project.path_with_namespace), "w") as f:
        print("Saving history")
        f.write(yaml.dump(result, sort_keys=True))

    print("Labels saved: {}".format(len(result_labels_saved)))
    print("Labels deleted: {}".format(len(result_labels_deleted)))
    expected_save_list = set(list(labels_to_save) + list(labels_to_reassign.values()))

    print("Missing: {}".format(expected_save_list - result_labels_saved))
