#!/usr/bin/env python3

import gitlab
import sys
import pprint
import yaml

global_data = None

with open("label-cleanup/global.yaml") as f:
    global_data = yaml.safe_load(f)

assert global_data is not None

GITLAB_URL = "https://gitlab.torproject.org/"
PRIVATE_TOKEN = ""

gitlab_client = gitlab.Gitlab(GITLAB_URL, PRIVATE_TOKEN, api_version=4)

gitlab_client.auth()

user = gitlab_client.user
print("Logged in as: {}".format(user.username))

projects = set()

for project in gitlab_client.projects.list(all=True):
    if project.path_with_namespace.startswith("tpo/"):
        projects.add(project)

ignore_projects = set([
    "tpo/anti-censorship/pluggable-transports/snowflake-mobile",
    "tpo/anti-censorship/pluggable-transports/snowflake-webext",
    "tpo/anti-censorship/team",
    "tpo/core/shadow",
    "tpo/core/torspec",
    "tpo/tpa/wiki-archive",
    "tpo/tpa/wiki-infra-archive",
])

result = {}

for project in projects:
    if project.path_with_namespace in ignore_projects:
        continue

    for label in project.labels.list(all=True, with_counts=True):
        want_to_delete = False

        if label.is_project_label:
            count = label.open_issues_count + label.closed_issues_count + label.open_merge_requests_count

            if count == 0:
                want_to_delete = True

        if want_to_delete:
            print("Deleting '{}' in {}".format(label.name, project.path_with_namespace))
            label.delete()
