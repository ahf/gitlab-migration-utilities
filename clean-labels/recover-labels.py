#!/usr/bin/env python3

import gitlab
import sys
import pprint
import yaml

LABEL_TO_RECOVER = "BugSmashFund"

GITLAB_URL = "https://gitlab.torproject.org/"
PRIVATE_TOKEN = ""

gitlab_client = gitlab.Gitlab(GITLAB_URL, PRIVATE_TOKEN, api_version=4)

gitlab_client.auth()

user = gitlab_client.user
print("Logged in as: {}".format(user.username))

projects = set()

for project in gitlab_client.projects.list(all=True):
    if project.path_with_namespace.startswith("tpo/"):
        projects.add(project)

ignore_projects = set([
    "tpo/anti-censorship/pluggable-transports/snowflake-mobile",
    "tpo/anti-censorship/pluggable-transports/snowflake-webext",
    "tpo/anti-censorship/team",
    "tpo/core/shadow",
    "tpo/core/torspec",
    "tpo/tpa/wiki-archive",
    "tpo/tpa/wiki-infra-archive",
])

# get all the projects in tpo.
for project in projects:
    if project.path_with_namespace in ignore_projects:
        continue
    tickets_to_assign_lost_label = set()

    # look for the history file for that project to get the tickets
    try:
        with open("label-cleanup/{}-history.yaml".format(project.path_with_namespace)) as f:
            input_data = yaml.safe_load(f)

            # get all the tickets that have the label to LABEL_TO_RECOVER
            for ticket in input_data:
                if LABEL_TO_RECOVER in input_data[ticket]['before']:
                    tickets_to_assign_lost_label.add(ticket)

    except Exception as e:
        print("Error: {}".format(e))

    # recover the label for all tickets_to_assign_lost_label
    for issue_id in tickets_to_assign_lost_label:
        issue = project.issues.get(issue_id)
        issue.labels.append(LABEL_TO_RECOVER)
        issue.save()
