This script was used as part of our label clean-up process after the migration.
The input files can be found in:

https://gitlab.torproject.org/ahf/label-cleanup/
